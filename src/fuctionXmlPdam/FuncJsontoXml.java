/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fuctionXmlPdam;

import functionXml.*;
import java.util.ArrayList;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
/**
 *
 * @author e-baddy
 */
public class FuncJsontoXml extends JsontoXmlPLN {

    @SuppressWarnings("empty-statement")
    public static StringBuilder getData(String dts, String staninput, String productin, String serviceunitin, StringBuilder datesett, int badmin) throws ParseException {
        JSONParser jp = new JSONParser();
        String dtjson =dts;
            //parse data
            Object object = jp.parse(dtjson);
            JSONObject jso = (JSONObject) object;

            //set to response xml
            JSONArray zoneList = (JSONArray) jso.get("respdata");

            String jsonStr = zoneList.get(0).toString();
            Object objecta = jp.parse(jsonStr);
            JSONObject dtrespdata = new JSONObject((Map) (objecta));

            Long amount = (Long) dtrespdata.get("tagihan");
            String nama = (String) dtrespdata.get("nama");
            String subscriber_segmentation = (String) dtrespdata.get("tarif");
            String switcher_refno = (String) jso.get("respid");
            String subscriber_id = (String) dtrespdata.get("idpel");
            String service_unit_phone = (String) dtrespdata.get("service_unit_phone");
            String power = (String) dtrespdata.get("daya");
            int payment_status = zoneList.size();
            Long saldo = (Long) jso.get("saldo");
            
            //convert date
            String dates = (String) dtrespdata.get("waktu_transaksi");
            String rsatu = dates.replace("-", "");
            String rdua = rsatu.replace(":", "");
            String datetime = rdua.replace(" ", "");

            
            int tbadmin = payment_status*badmin;
            
            
            
            //set stand_meter_summary
            StringBuilder stan = new StringBuilder("");
            if(payment_status ==1){               
                String stan_awal = (String) dtrespdata.get("stan_awal");
                String stan_akhir = (String) dtrespdata.get("stan_akhir");
                
                //append data
                stan.append(stan_awal+" - "+stan_akhir);
            }else{
                int max = payment_status-1;
                //get data stan awal
                String jsonStrmin = zoneList.get(0).toString();
                Object objectamin = jp.parse(jsonStrmin);
                JSONObject dtrespdatamin = new JSONObject((Map) (objectamin));
                //set value stan awal
                String stan_awalmin = (String) dtrespdatamin.get("stan_awal");

                //get data stan akhir
                String jsonStrmax = zoneList.get(max).toString();
                Object objectamax = jp.parse(jsonStrmax);
                JSONObject dtrespdatamax = new JSONObject((Map) (objectamax));
                //set value stan aakhir
                String stan_akhirmax = (String) dtrespdatamax.get("stan_akhir");

                //append data
                stan.append(stan_awalmin+" - "+stan_akhirmax);
            }
            
            //data bills
            StringBuilder getbills = dataRes(zoneList);
            String billstostr = getbills.toString();
            String arrt[]= billstostr.split("@@");
            
            StringBuilder amt = new StringBuilder("");;
            for (int i=1; i < arrt.length; i++) {
                amt.append(Integer.valueOf(arrt[i]));
            }
            Long amounttotal = Long.valueOf(amt.toString());;
            
            long tamount = amounttotal+tbadmin;
            
            StringBuilder bills = new StringBuilder("");;
            for (int i=0; i < 1; i++) {
                bills.append(arrt[i]);
            }
            
            //tag harga
            Long Dharga = null;
            if(payment_status ==1){
                Dharga = amounttotal+1000;
            }else{
                Dharga = amounttotal+1000;
            }
            
            //data blth summary
            StringBuilder blthSummary = blthSummary(zoneList);
            
            StringBuilder blthSm = new StringBuilder("");
            if(payment_status ==1){
                String dtsm = blthSummary.substring(0,5);
                blthSm.append(dtsm);
            }else{
                String dtsm = blthSummary.substring(0,19);
                blthSm.append(dtsm);
            }
            
            StringBuilder dataConvert = new StringBuilder("");
            dataConvert.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
            dataConvert.append("<response>");
            dataConvert.append("<produk>"+productin+"</produk>");
            dataConvert.append("<amount>" + tamount + "</amount>");
            dataConvert.append("<stan>"+staninput+"</stan>");
            dataConvert.append("<datetime>" + datetime + "</datetime>");
            dataConvert.append("<date_settlement>00000000</date_settlement>");
            dataConvert.append("<merchant_code>6021</merchant_code>");
            dataConvert.append("<bank_code>4510017</bank_code>");
            dataConvert.append("<rc>0000</rc>");
            dataConvert.append("<terminal_id>0000000000000048</terminal_id>");
            dataConvert.append("<switcher_id>SWID003</switcher_id>");
            dataConvert.append("<bill_status>" + payment_status + "</bill_status>");
            dataConvert.append("<outstanding_bill>0" + payment_status + "</outstanding_bill>");
            dataConvert.append("<payment_status>" + payment_status + "</payment_status>");
            dataConvert.append("<subscriber_id>" + subscriber_id + "</subscriber_id>");
            dataConvert.append("<switcher_refno>" + switcher_refno + "</switcher_refno>");
            dataConvert.append("<subscriber_name>" + nama + " </subscriber_name>");
            dataConvert.append("<service_unit>"+serviceunitin+"</service_unit>");
            dataConvert.append("<service_unit_phone>" + service_unit_phone + " </service_unit_phone>");
            dataConvert.append("<subscriber_segmentation> " + subscriber_segmentation + "</subscriber_segmentation>");
            dataConvert.append("<power>000000" + power + "</power>");
            dataConvert.append("<admin_charge>"+tbadmin+"</admin_charge>");
            dataConvert.append("<info_text>000000000</info_text>");
            dataConvert.append("<bills>");
            dataConvert.append(bills);
            dataConvert.append("</bills>");
            dataConvert.append("<blth_summary>" + blthSm + "</blth_summary>");
            dataConvert.append("<stand_meter_summary>"+stan+"</stand_meter_summary>");
            dataConvert.append("<bk>0</bk>");
            dataConvert.append("<rptag>" + amounttotal + "</rptag>");
            dataConvert.append("<saldo>"+saldo+"</saldo>");
            dataConvert.append("<harga>" + Dharga + "</harga>");
            dataConvert.append("</response>");
            
            return dataConvert;
    }
    
    public static StringBuilder dataRes(JSONArray zoneList) throws ParseException {
        JSONParser jp = new JSONParser();
        StringBuffer sb = new StringBuffer("");

        ArrayList<String> arrlist = new ArrayList<String>(5);
        for (int i = 0; i < zoneList.size(); i++) {
            String jsonStr = zoneList.get(i).toString();
            arrlist.add(jsonStr);
        }

        // let us print all the elements available in list
        ArrayList<Long> tamount = new ArrayList<Long>(5);
        StringBuilder dataRespdata = new StringBuilder("");
        for (String data : arrlist) {
            Object objecta = jp.parse(data);
            JSONObject json = new JSONObject((Map) (objecta));
            //set date
            String waktu_transaksi = (String) json.get("waktu_transaksi");
            String rsatu = waktu_transaksi.replace("-", "");
            String rdua = rsatu.replace(":", "");
            String datetimae = rdua.replace(" ", "");

            String blth = (String) json.get("blth");
            Long total_electricity_bill = (Long) json.get("tagihan");
            String stan_awal = (String) json.get("stan_awal");
            String stan_akhir = (String) json.get("stan_akhir");

            SwitchthBulan getthBln = new SwitchthBulan();
            String bill_period = getthBln.findThBulan(blth);
            
            //append data
            StringBuilder dataRsp = new StringBuilder("");
            dataRsp.append("<bill>");
            dataRsp.append("<bill_period>" + bill_period + "</bill_period>");
            dataRsp.append("<due_date></due_date>");
            dataRsp.append("<meter_read_date></meter_read_date>");
            dataRsp.append("<total_electricity_bill>000000" + total_electricity_bill + "</total_electricity_bill>");
            dataRsp.append("<incentive>00000000000</incentive>");
            dataRsp.append("<value_added_tax>0000000000</value_added_tax>");
            dataRsp.append("<penalty_fee>000000000000</penalty_fee>");
            dataRsp.append("<previous_meter_reading1>" + stan_awal + "</previous_meter_reading1>");
            dataRsp.append("<current_meter_reading1>" + stan_akhir + "</current_meter_reading1>");
            dataRsp.append("<previous_meter_reading2>00000000</previous_meter_reading2>");
            dataRsp.append("<current_meter_reading2>00000000</current_meter_reading2>");
            dataRsp.append("<previous_meter_reading3>00000000</previous_meter_reading3>");
            dataRsp.append("<current_meter_reading3>00000000</current_meter_reading3>");
            dataRsp.append("</bill>");

            dataRespdata.append(dataRsp);
            tamount.add(total_electricity_bill);
        }
        
        //sum data amount
        long sum = 0;
        for (Long i : tamount) {
             sum = sum+i;
        }
        
        StringBuilder dataResp = new StringBuilder(dataRespdata);
        dataResp.append("@@"+sum);
//        return dataRespdata;
        return dataResp;
    }

    public static StringBuilder blthSummary(JSONArray zoneList) throws ParseException {
        JSONParser jp = new JSONParser();
        StringBuffer sb = new StringBuffer("");

        ArrayList<String> arrlist = new ArrayList<String>(5);
        for (int i = 0; i < zoneList.size(); i++) {
            String jsonStr = zoneList.get(i).toString();
            arrlist.add(jsonStr);
        }

        // let us print all the elements available in list
        StringBuilder blthSumm = new StringBuilder("");
        for (String data : arrlist) {
            Object objecta = jp.parse(data);
            JSONObject json = new JSONObject((Map) (objecta));

            String blth = (String) json.get("blth");

            //append data
            blthSumm.append(blth + ", ");
        }
        return blthSumm;
    }
}
