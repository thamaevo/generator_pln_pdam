/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fuctionXmlPdam;
import functionXml.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author e-baddy
 */
public class SwitchthBulan {  
    
    public static String findThBulan(String blth){
        String data = blth;
        String buln = data.substring(0,3);
        String thn = data.substring(Math.max(0, data.length() - 2));

        String findbln = findBulan(buln);
        
        
        String tahunbln = "20"+thn+findbln;
        return tahunbln;
    }
    
    private static String findBulan(String buln) {
        String bln = buln;
        String dayString = null;
        switch (bln) {
            case "JAN":
                dayString = "01";
                break;
            case "FEB":
                dayString = "02";
                break;
            case "MAR":
                dayString = "03";
                break;
            case "APR":
                dayString = "04";
                break;
            case "MEI":
                dayString = "05";
                break;
            case "JUN":
                dayString = "06";
                break;
            case "JUL":
                dayString = "07";
                break;
            case "AGS":
                dayString = "08";
                break;
            case "SEP":
                dayString = "09";
                break;
            case "OKT":
                dayString = "10";
                break;
            case "NOV":
                dayString = "11";
                break;
            case "DES":
                dayString = "12";
                break;
            default :
            dayString = "99";
        }
        return dayString;
    }


}
