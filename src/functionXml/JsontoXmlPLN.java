/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionXml;

import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author e-baddy
 */
public class JsontoXmlPLN {

    public static void main(String[] args) throws Exception {
        JSONParser jp = new JSONParser();
        String dtjson = "{\"response\":\"SWT:0000\",\"respid\":\"042621CAD19BEBEBB36970FE1DDE7CCE\",\"respdata\":[{\"idpel\":\"212100084914\",\"nama\":\"MUSLIMIN ABDULLAH\",\"blth\":\"JAN19\",\"tarif\":\"R1\",\"daya\":\"450\",\"tagihan\":256908,\"admin\":2750,\"total\":259658,\"stan_awal\":\"00040675\",\"stan_akhir\":\"00041161\",\"sisa_tungg\":\"2\",\"billreff\":\"042621CAD19BEBEBB36970FE1DDE7CCE\",\"service_unit_phone\":\"123\",\"waktu_transaksi\":\"2019-03-26 08:59:59\"},{\"idpel\":\"212100084914\",\"nama\":\"MUSLIMIN ABDULLAH\",\"blth\":\"FEB19\",\"tarif\":\"R1\",\"daya\":\"450\",\"tagihan\":233252,\"admin\":2750,\"total\":236002,\"stan_awal\":\"00041161\",\"stan_akhir\":\"00041608\",\"sisa_tungg\":\"1\",\"billreff\":\"042621CAD19BEBEBB36970FE1DDE7CCE\",\"service_unit_phone\":\"123\",\"waktu_transaksi\":\"2019-03-26 08:59:59\"},{\"idpel\":\"212100084914\",\"nama\":\"MUSLIMIN ABDULLAH\",\"blth\":\"MAR19\",\"tarif\":\"R1\",\"daya\":\"450\",\"tagihan\":225485,\"admin\":2750,\"total\":228235,\"stan_awal\":\"00041608\",\"stan_akhir\":\"00042046\",\"sisa_tungg\":\"0\",\"billreff\":\"042621CAD19BEBEBB36970FE1DDE7CCE\",\"service_unit_phone\":\"123\",\"waktu_transaksi\":\"2019-03-26 08:59:59\"}],\"info_struk\":\"\\u001b@||PT. BANK BUKOPIN, TBK||40010008\\/PELANGI|2019-03-26 08:59:59|STRUK PEMBAYARAN TAGIHAN LISTRIK|IDPEL      : 212100084914|NAMA       : MUSLIMIN ABDULLAH|TARIF\\/DAYA : R1\\/450|BL\\/TH      : JAN19|STAND METER: 00040675-00041161|RP TAG PLN : 256,908|NO REF       :|042621CAD19BEBEBB36970FE1DDE7CCE|PLN menyatakan struk ini|sebagai bukti pembayaran|yang sah.|Anda masih memiliki|sisa tunggakan 2 bulan|ADMIN BANK :   2,750|TOTAL BAYAR: 259,658|TERIMAKASIH|Rincian tagihan dapat diakses|di www.pln.co.id|Informasi Hubungi Call Center : 123|CU\\/pelangi40010008\\/20190327142234|5866560ca0aba13319f37ffa72f60729|||PT. BANK BUKOPIN, TBK||40010008\\/PELANGI|2019-03-26 08:59:59|STRUK PEMBAYARAN TAGIHAN LISTRIK|IDPEL      : 212100084914|NAMA       : MUSLIMIN ABDULLAH|TARIF\\/DAYA : R1\\/450|BL\\/TH      : FEB19|STAND METER: 00041161-00041608|RP TAG PLN : 233,252|NO REF       :|042621CAD19BEBEBB36970FE1DDE7CCE|PLN menyatakan struk ini|sebagai bukti pembayaran|yang sah.|Anda masih memiliki|sisa tunggakan 1 bulan|ADMIN BANK :   2,750|TOTAL BAYAR: 236,002|TERIMAKASIH|Rincian tagihan dapat diakses|di www.pln.co.id|Informasi Hubungi Call Center : 123|CU\\/pelangi40010008\\/20190327142234|5866560ca0aba13319f37ffa72f60729|||PT. BANK BUKOPIN, TBK||40010008\\/PELANGI|2019-03-26 08:59:59|STRUK PEMBAYARAN TAGIHAN LISTRIK|IDPEL      : 212100084914|NAMA       : MUSLIMIN ABDULLAH|TARIF\\/DAYA : R1\\/450|BL\\/TH      : MAR19|STAND METER: 00041608-00042046|RP TAG PLN : 225,485|NO REF       :|042621CAD19BEBEBB36970FE1DDE7CCE|PLN menyatakan struk ini|sebagai bukti pembayaran|yang sah.|ADMIN BANK :   2,750|TOTAL BAYAR: 228,235|TERIMAKASIH|Rincian tagihan dapat diakses|di www.pln.co.id|Informasi Hubungi Call Center : 123|CU\\/pelangi40010008\\/20190327142234|5866560ca0aba13319f37ffa72f60729|||\",\"saldo\":19414792029}";
//        String dtjson = "{\"response\":\"SWT:0000\",\"respid\":\"042621CAD19BE255DB15C7509C338551\",\"respdata\":[{\"idpel\":\"141202418558\",\"nama\":\"MEIJI.P\",\"blth\":\"MAR19\",\"tarif\":\"R1\",\"daya\":\"1300\",\"tagihan\":529031,\"admin\":2750,\"total\":531781,\"stan_awal\":\"00028468\",\"stan_akhir\":\"00028803\",\"sisa_tungg\":\"0\",\"billreff\":\"042621CAD19BE255DB15C7509C338551\",\"service_unit_phone\":\"123\",\"waktu_transaksi\":\"2019-03-26 07:59:08\"}],\"info_struk\":\"\\u001b@||PT. BANK BUKOPIN, TBK||40010008\\/PELANGI|2019-03-26 07:59:08|STRUK PEMBAYARAN TAGIHAN LISTRIK|IDPEL      : 141202418558|NAMA       : MEIJI.P|TARIF\\/DAYA : R1\\/1300|BL\\/TH      : MAR19|STAND METER: 00028468-00028803|RP TAG PLN : 529,031|NO REF       :|042621CAD19BE255DB15C7509C338551|PLN menyatakan struk ini|sebagai bukti pembayaran|yang sah.|ADMIN BANK :   2,750|TOTAL BAYAR: 531,781|TERIMAKASIH|Rincian tagihan dapat diakses|di www.pln.co.id|Informasi Hubungi Call Center : 123|CU\\/pelangi40010008\\/20190327142150|8cbbc51c32e09e7615d2d2627f5d0301|||\",\"saldo\":19415245159}";

        try {
            //parse data
            Object object = jp.parse(dtjson);
            JSONObject jso = (JSONObject) object;

            //set to response xml
            JSONArray zoneList = (JSONArray) jso.get("respdata");

            String jsonStr = zoneList.get(0).toString();
            Object objecta = jp.parse(jsonStr);
            JSONObject dtrespdata = new JSONObject((Map) (objecta));

            Long amount = (Long) dtrespdata.get("tagihan");
            String nama = (String) dtrespdata.get("nama");
            String subscriber_segmentation = (String) dtrespdata.get("tarif");
            String switcher_refno = (String) dtrespdata.get("respid");
            String subscriber_id = (String) dtrespdata.get("idpel");
            String service_unit_phone = (String) dtrespdata.get("service_unit_phone");
            String power = (String) dtrespdata.get("daya");
            Long admin = (Long) dtrespdata.get("admin");
            int payment_status = zoneList.size();
            Long saldo = (Long) jso.get("saldo");
            //convert date
            String dates = (String) dtrespdata.get("waktu_transaksi");
            String rsatu = dates.replace("-", "");
            String rdua = rsatu.replace(":", "");
            String datetime = rdua.replace(" ", "");
            
            StringBuilder stan = new StringBuilder("");
            if(payment_status ==1){               
                String stan_awal = (String) dtrespdata.get("stan_awal");
                String stan_akhir = (String) dtrespdata.get("stan_akhir");
                
                //append data
                stan.append(stan_awal+" - "+stan_akhir);
            }else{
                int max = payment_status-1;
                
                String jsonStrmax = zoneList.get(max).toString();
                Object objectamax = jp.parse(jsonStrmax);
                JSONObject dtrespdatamax = new JSONObject((Map) (objectamax));
                
                String stan_awalmax = (String) dtrespdatamax.get("stan_awal");
                String stan_akhirmax = (String) dtrespdatamax.get("stan_akhir");
                
                //append data
                stan.append(stan_awalmax+" - "+stan_akhirmax);
            }
            
            //data bills
            StringBuilder bills = dataRes(zoneList);

            //data blth summary
            StringBuilder blthSummary = blthSummary(zoneList);
            
            StringBuilder blthSm = new StringBuilder("");
            if(payment_status ==1){
                String dtsm = blthSummary.substring(0,5);
                blthSm.append(dtsm);
            }else{
                String dtsm = blthSummary.substring(0,19);
                blthSm.append(dtsm);
            }
            
            StringBuilder dataConvert = new StringBuilder("");
            dataConvert.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
            dataConvert.append("<response>");
            dataConvert.append("<produk>PLNPOSTPAID</produk>");
            dataConvert.append("<amount>" + amount + "</amount>");
            dataConvert.append("<stan>000203735904</stan>");
            dataConvert.append("<datetime>" + datetime + "</datetime>");
            dataConvert.append("<date_settlement>00000000</date_settlement>");
            dataConvert.append("<merchant_code>6021</merchant_code>");
            dataConvert.append("<bank_code>4510017</bank_code>");
            dataConvert.append("<rc>0000</rc>");
            dataConvert.append("<terminal_id>0000000000000048</terminal_id>");
            dataConvert.append("<switcher_id>SWID003</switcher_id>");
            dataConvert.append("<bill_status>" + payment_status + "</bill_status>");
            dataConvert.append("<outstanding_bill>0" + payment_status + "</outstanding_bill>");
            dataConvert.append("<payment_status>" + payment_status + "</payment_status>");
            dataConvert.append("<subscriber_id>" + subscriber_id + "</subscriber_id>");
            dataConvert.append("<switcher_refno>" + switcher_refno + "</switcher_refno>");
            dataConvert.append("<subscriber_name>" + nama + "</subscriber_name>");
            dataConvert.append("<service_unit></service_unit>");
            dataConvert.append("<service_unit_phone>" + service_unit_phone + "</service_unit_phone>");
            dataConvert.append("<subscriber_segmentation>" + subscriber_segmentation + "</subscriber_segmentation>");
            dataConvert.append("<power>00000" + power + "</power>");
            dataConvert.append("<admin_charge>"+admin+"</admin_charge>");
            dataConvert.append("<info_text>000000000</info_text>");
            dataConvert.append(bills);
            dataConvert.append("<blth_summary>" + blthSm + "</blth_summary>");
            dataConvert.append("<stand_meter_summary>"+stan+"</stand_meter_summary>");
            dataConvert.append("<bk>0</bk>");
            dataConvert.append("<rptag>" + amount + "</rptag>");
            dataConvert.append("<saldo>"+saldo+"</saldo>");
            dataConvert.append("<harga>" + amount + "</harga>");
            dataConvert.append("</response>");

            System.out.println(dataConvert);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }
    }

    public static StringBuilder dataRes(JSONArray zoneList) throws ParseException {
        JSONParser jp = new JSONParser();
        StringBuffer sb = new StringBuffer("");

        ArrayList<String> arrlist = new ArrayList<String>(5);
        for (int i = 0; i < zoneList.size(); i++) {
            String jsonStr = zoneList.get(i).toString();
            arrlist.add(jsonStr);
        }

        // let us print all the elements available in list
        StringBuilder dataRespdata = new StringBuilder("");
        for (String data : arrlist) {
            Object objecta = jp.parse(data);
            JSONObject json = new JSONObject((Map) (objecta));
            //set date
            String waktu_transaksi = (String) json.get("waktu_transaksi");
            String rsatu = waktu_transaksi.replace("-", "");
            String rdua = rsatu.replace(":", "");
            String datetimae = rdua.replace(" ", "");

            String blth = (String) json.get("blth");
            Long total_electricity_bill = (Long) json.get("tagihan");
            String stan_awal = (String) json.get("stan_awal");
            String stan_akhir = (String) json.get("stan_akhir");

            //append data
            StringBuilder dataRsp = new StringBuilder("");
            dataRsp.append("<bills>");
            dataRsp.append("<bill>");
            dataRsp.append("<bill_period>" + datetimae.substring(0, 6) + "</bill_period>");
            dataRsp.append("<due_date></due_date>");
            dataRsp.append("<meter_read_date></meter_read_date>");
            dataRsp.append("<total_electricity_bill>000000" + total_electricity_bill + "</total_electricity_bill>");
            dataRsp.append("<incentive>00000000000</incentive>");
            dataRsp.append("<value_added_tax>0000000000</value_added_tax>");
            dataRsp.append("<penalty_fee>000000000000</penalty_fee>");
            dataRsp.append("<previous_meter_reading1>" + stan_awal + "</previous_meter_reading1>");
            dataRsp.append("<current_meter_reading1>" + stan_awal + "</current_meter_reading1>");
            dataRsp.append("<previous_meter_reading2>00000000</previous_meter_reading2>");
            dataRsp.append("<current_meter_reading2>00000000</current_meter_reading2>");
            dataRsp.append("<previous_meter_reading3>00000000</previous_meter_reading3>");
            dataRsp.append("<current_meter_reading3>00000000</current_meter_reading3>");
            dataRsp.append("</bill>");
            dataRsp.append("</bills>");

            dataRespdata.append(dataRsp);
        }

        return dataRespdata;
    }

    public static StringBuilder blthSummary(JSONArray zoneList) throws ParseException {
        JSONParser jp = new JSONParser();
        StringBuffer sb = new StringBuffer("");

        ArrayList<String> arrlist = new ArrayList<String>(5);
        for (int i = 0; i < zoneList.size(); i++) {
            String jsonStr = zoneList.get(i).toString();
            arrlist.add(jsonStr);
        }

        // let us print all the elements available in list
        StringBuilder blthSumm = new StringBuilder("");
        for (String data : arrlist) {
            Object objecta = jp.parse(data);
            JSONObject json = new JSONObject((Map) (objecta));

            String blth = (String) json.get("blth");

            //append data
            blthSumm.append(blth + ", ");
        }
        return blthSumm;
    }
}
