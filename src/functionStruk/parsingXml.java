/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionStruk;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import static java.lang.Integer.parseInt;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author e-baddy
 */
public class parsingXml {

    public String startParsingPart(String xml) throws ParserConfigurationException, SAXException, IOException {
        String status="";
        
        try{
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputStream xmlAkhir = new ByteArrayInputStream(xml.toString().getBytes());
            Document doc = dBuilder.parse(xmlAkhir);
            doc.getDocumentElement().normalize();
            NodeList Listres = doc.getElementsByTagName("response");
            NodeList Listbill = doc.getElementsByTagName("bill");

            //SET PARAMETER CONVERT TO IMAGE
            Node nNode = Listres.item(0);
            Element eElement = (Element) nNode;
            String idPel = eElement.getElementsByTagName("subscriber_id").item(0).getTextContent();
            String nmPel = eElement.getElementsByTagName("subscriber_name").item(0).getTextContent();
            String trf = eElement.getElementsByTagName("subscriber_segmentation").item(0).getTextContent();
            String dy = eElement.getElementsByTagName("power").item(0).getTextContent();
            String swref = eElement.getElementsByTagName("switcher_refno").item(0).getTextContent();
            String jBulan = eElement.getElementsByTagName("blth_summary").item(0).getTextContent();
            String jAdmin = eElement.getElementsByTagName("admin_charge").item(0).getTextContent();

            //START CONVERT TO IMAGE
            convertImg cImg = new convertImg();
            cImg.startConvert(Listbill,idPel,nmPel,trf,dy,swref,jBulan,jAdmin);
            status = "sukses";
        }catch (IOException ex) {
            ex.printStackTrace();
            status = "gagal";
        }    
        
        return status;
    }
    
    public String startParsingFull(String xml) throws ParserConfigurationException, SAXException, IOException {
        String status="";
        
        try{
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputStream xmlAkhir = new ByteArrayInputStream(xml.toString().getBytes());
            Document doc = dBuilder.parse(xmlAkhir);
            doc.getDocumentElement().normalize();
            NodeList Listres = doc.getElementsByTagName("response");
            NodeList Listbill = doc.getElementsByTagName("bill");

            //SET PARAMETER CONVERT TO IMAGE
            Node nNode          = Listres.item(0);
            Element eElement    = (Element) nNode;
            String idPel        = eElement.getElementsByTagName("subscriber_id").item(0).getTextContent();
            String nmPel        = eElement.getElementsByTagName("subscriber_name").item(0).getTextContent();
            String trf          = eElement.getElementsByTagName("subscriber_segmentation").item(0).getTextContent();
            String dy           = eElement.getElementsByTagName("power").item(0).getTextContent();
            String smMeter      = eElement.getElementsByTagName("stand_meter_summary").item(0).getTextContent();
            String rptag        = eElement.getElementsByTagName("rptag").item(0).getTextContent();
            String AdminC       = eElement.getElementsByTagName("admin_charge").item(0).getTextContent();
                int sum         = parseInt(rptag)+parseInt(AdminC);
            String totBayar     = String.valueOf(sum);
            String swRef        = eElement.getElementsByTagName("switcher_refno").item(0).getTextContent();
            String jBln       = eElement.getElementsByTagName("blth_summary").item(0).getTextContent().replace("19", "2019");
                String bln[] =jBln.split(",");
                int sumAdmin = parseInt(AdminC)*bln.length;
            String jAdmin     = String.valueOf(sumAdmin);
            //START CONVERT TO IMAGE
            convertImg cImg = new convertImg();
            cImg.startConvertFull(idPel,nmPel,trf,dy,smMeter,rptag,jAdmin,totBayar,swRef,jBln);
            status = "sukses";
            
        }catch (IOException ex) {
            ex.printStackTrace();
            status = "gagal";
        }    
        
        return status;
    }
    
}
