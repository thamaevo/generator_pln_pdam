/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionStruk;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import javax.imageio.ImageIO;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author e-baddy
 */
public class convertImg {

    public void startConvert(NodeList Listbills, String idPel, String nmPel, String trf, String dy, String swRef, String jBulan, String jAdmin) {

        for (int temp = 0; temp < Listbills.getLength(); temp++) {
            Node nNode = Listbills.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;

                //SET UKURAN IMAGE
                int width = 1060;
                int height = 250;

                //CALL CLASS CONVERT
                BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
                Graphics2D g2d = img.createGraphics();
                Font font = new Font("Courier New", Font.PLAIN, 12);
                FontMetrics fm = g2d.getFontMetrics();
                fm = g2d.getFontMetrics();
                g2d = img.createGraphics();

                // SET COLOR BACKGROUD
                g2d.setBackground(Color.WHITE);
                g2d.clearRect(0, 0, width, height);

                // SET COLOR TEXT
                g2d.setColor(Color.BLACK);
                g2d.setFont(font);

                //SET PARAMETER
                String vBulan = eElement.getElementsByTagName("bill_period").item(0).getTextContent();
                String vSm1 = eElement.getElementsByTagName("previous_meter_reading1").item(0).getTextContent();
                String vSm2 = eElement.getElementsByTagName("current_meter_reading1").item(0).getTextContent();
                String vRptag = eElement.getElementsByTagName("total_electricity_bill").item(0).getTextContent();

                //SET CURRENCY
                double drptag = Double.parseDouble(String.valueOf(vRptag));
                int dtbayar = parseInt(vRptag) + parseInt(jAdmin);
                double Badmin = Double.parseDouble(String.valueOf(jAdmin));
                double tBayar = Double.parseDouble(String.valueOf(dtbayar));;

                //SET MOUNTH AND YEAR
                findThBulan getthBln = new findThBulan();
                String blnTh = getthBln.findThBulan(vBulan);

                //SET VALUE UNTUK SATU BULAN BAGIAN KIRI
                g2d.drawString("STRUK PEMBAYARAN PLN POSTPAID", 50, 20);
                g2d.drawString("IDPEL        : " + idPel, 30, 55);
                g2d.drawString("NAMA         : " + nmPel, 30, 70);
                g2d.drawString("TRF/DY       : " + trf.trim() + " / " + dy + "  VA", 30, 85);
                g2d.drawString("ST METER     : " + vSm1 + "-" + vSm2, 30, 100);
                g2d.drawString("RP TAG PLN   : Rp. " + String.format("%,.2f", drptag), 30, 115);
                g2d.drawString("ADMIN BANK   : Rp. " + String.format("%,.2f", Badmin), 30, 130);
                g2d.drawString("TOTAL BAYAR  : Rp. " + String.format("%,.2f", tBayar), 30, 145);
                g2d.drawString("POSTPAID PLN", 80, 160);
                g2d.drawString("SW REF       : ", 30, 175);
                g2d.drawString(swRef, 30, 190);
                g2d.drawString("*CU", 30, 205);

                //SET VALUE UNTUK SATU BULAN BAGIAN KANAN
                g2d.drawString("STRUK PEMBAYARAN TAGIHAN LISTRIK", 640, 20);
                g2d.drawString("IDPEL        : " + idPel, 500, 55);
                //--- 
                g2d.drawString("BL / TH      : " + blnTh, 800, 55);
                g2d.drawString("ST METER     : " + vSm1 + "-" + vSm2, 800, 70);
                //--- 
                g2d.drawString("NAMA         : " + nmPel, 500, 70);
                g2d.drawString("TARIF DAYA   : " + trf.trim() + " / " + dy + "  VA", 500, 85);
                g2d.drawString("RP TAG PLN   : Rp. " + String.format("%,.2f", drptag), 500, 100);
                g2d.drawString("SW REF       : " + swRef, 500, 115);
                g2d.drawString("PLN menyatakan struk ini sebagai alat bukti pembayaran yang sah.", 530, 130);
                g2d.drawString("ADMIN BANK   : Rp. " + String.format("%,.2f", Badmin), 500, 145);
                g2d.drawString("TOTAL BAYAR  : Rp. " + String.format("%,.2f", tBayar), 500, 160);
                //tanda terima
                g2d.drawString("Terimakasih", 700, 190);
                g2d.drawString("Rincian tagihan dapat diakses di www.pln.co.id", 580, 205);
                g2d.drawString("Informasi Hubungi Call Center : 123 atau Hub. PLN Terdekat.", 530, 220);
                g2d.drawString("*CU", 730, 235);
                g2d.dispose();

                //SAVE IMAGE
                System.out.println("------------------ LOG SAVE IMAGE " + blnTh + ".jpg ------------------");
                saveImg(img, idPel+"-"+nmPel.trim()+"-"+blnTh);

                //SET LOG DATA
                System.out.println("PERIODE : " + eElement.getElementsByTagName("bill_period").item(0).getTextContent());
            }
        }

    }

    public void startConvertFull(String idPel, String nmPel, String trf, String dy, String smMeter, String rptag, String jAdmin, String totBayar, String swRef, String jBln) {
        //SET UKURAN IMAGE
        int width = 1200;
        int height = 250;

        //CALL CLASS CONVERT
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D g2d = img.createGraphics();
        Font font = new Font("Courier New", Font.PLAIN, 12);
        FontMetrics fm = g2d.getFontMetrics();
        fm = g2d.getFontMetrics();
        g2d = img.createGraphics();

        // SET COLOR BACKGROUD
        g2d.setBackground(Color.WHITE);
        g2d.clearRect(0, 0, width, height);

        // SET COLOR TEXT
        g2d.setColor(Color.BLACK);
        g2d.setFont(font);

        //SET CURRENCY
        double drptag = Double.parseDouble(String.valueOf(rptag));
        double Badmin = Double.parseDouble(String.valueOf(jAdmin));
        double tBayar = Double.parseDouble(String.valueOf(totBayar));;

        //SET VALUE UNTUK SATU BULAN BAGIAN KIRI
        g2d.drawString("STRUK PEMBAYARAN PLN POSTPAID", 60, 20);
        g2d.drawString("IDPEL        : " + idPel, 30, 55);
        g2d.drawString("NAMA         : " + nmPel, 30, 70);
        g2d.drawString("TRF/DY       : " + trf.trim() + " / " + dy + "  VA", 30, 85);
        g2d.drawString("ST METER     : " + smMeter, 30, 100);
        g2d.drawString("RP TAG PLN   : Rp. " + String.format("%,.2f", drptag), 30, 115);
        g2d.drawString("ADMIN BANK   : Rp. " + String.format("%,.2f", Badmin), 30, 130);
        g2d.drawString("TOTAL BAYAR  : Rp. " + String.format("%,.2f", tBayar), 30, 145);
        g2d.drawString("POSTPAID PLN", 80, 160);
        g2d.drawString("SW REF       : ", 30, 175);
        g2d.drawString(swRef, 30, 190);
        g2d.drawString("*CU", 30, 205);

        //SET VALUE UNTUK SATU BULAN BAGIAN KANAN
        g2d.drawString("STRUK PEMBAYARAN TAGIHAN LISTRIK", 680, 20);
        g2d.drawString("IDPEL        : " + idPel, 500, 55);
        //--- 
        g2d.drawString("BL / TH      : " + jBln, 800, 55);
        g2d.drawString("ST METER     : " + smMeter, 800, 70);
        //--- 
        g2d.drawString("NAMA         : " + nmPel, 500, 70);
        g2d.drawString("TARIF DAYA   : " + trf.trim() + " / " + dy + "  VA", 500, 85);
        g2d.drawString("RP TAG PLN   : Rp. " + String.format("%,.2f", drptag), 500, 100);
        g2d.drawString("SW REF       : " + swRef, 500, 115);
        g2d.drawString("PLN menyatakan struk ini sebagai alat bukti pembayaran yang sah.", 580, 130);
        g2d.drawString("ADMIN BANK   : Rp. " + String.format("%,.2f", Badmin), 500, 145);
        g2d.drawString("TOTAL BAYAR  : Rp. " + String.format("%,.2f", tBayar), 500, 160);
        //tanda terima
        g2d.drawString("Terimakasih", 730, 190);
        g2d.drawString("Rincian tagihan dapat diakses di www.pln.co.id", 610, 205);
        g2d.drawString("Informasi Hubungi Call Center : 123 atau Hub. PLN Terdekat.", 560, 220);
        g2d.drawString("*CU", 760, 235);
        g2d.dispose();

        //SAVE IMAGE
        System.out.println("------------------ LOG SAVE IMAGE STRUK-FULL.jpg ------------------");
        saveImg(img, idPel+"-"+nmPel.trim()+"-FULL");

        //SET LOG DATA
        System.out.println("PERIODE : " + jBln);
    }

    public void saveImg(BufferedImage img, String Bulan) {
        String status = "";
        try {
            ImageIO.write(img, "png", new File("STRUK-" + Bulan + ".jpg"));
            System.out.println("Simpan gambar berhasil status = SUKSES");
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("Simpan gambar GAGAL status = GAGAL");
        }
    }
}
