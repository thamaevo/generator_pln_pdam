/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionStrukPdam;

import functionStrukPdam.*;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import static java.lang.Integer.parseInt;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author e-baddy
 */
public class parsingXmlPdam {

    public String startParsingPart(String xml) throws ParserConfigurationException, SAXException, IOException {
        String status="";
        
        try{
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputStream xmlAkhir = new ByteArrayInputStream(xml.toString().getBytes());
            Document doc = dBuilder.parse(xmlAkhir);
            doc.getDocumentElement().normalize();
            NodeList Listres = doc.getElementsByTagName("response");
            NodeList Listbill = doc.getElementsByTagName("bill");

            //SET PARAMETER CONVERT TO IMAGE
            Node nNode = Listres.item(0);
            Element eElement = (Element) nNode;
            String idPel = eElement.getElementsByTagName("idpel").item(0).getTextContent();
            String nmPel = eElement.getElementsByTagName("name").item(0).getTextContent();
            String swref = eElement.getElementsByTagName("switching_ref").item(0).getTextContent();
            String jBulan = eElement.getElementsByTagName("blth").item(0).getTextContent();
            String jAdmin = eElement.getElementsByTagName("biaya_admin").item(0).getTextContent();
            String jLunas = eElement.getElementsByTagName("waktu_lunas").item(0).getTextContent();

            //START CONVERT TO IMAGE
            convertImgPdam cImg = new convertImgPdam();
            cImg.startConvert(Listbill,idPel,nmPel,swref,jBulan,jAdmin,jLunas);
            status = "sukses";
        }catch (IOException ex) {
            ex.printStackTrace();
            status = "gagal";
        }    
        
        return status;
    }
    
    public String startParsingFull(String xml) throws ParserConfigurationException, SAXException, IOException {
        String status="";
        
        try{
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputStream xmlAkhir = new ByteArrayInputStream(xml.toString().getBytes());
            Document doc = dBuilder.parse(xmlAkhir);
            doc.getDocumentElement().normalize();
            NodeList Listres = doc.getElementsByTagName("response");
            NodeList Listbill = doc.getElementsByTagName("bill");

            //SET PARAMETER CONVERT TO IMAGE
            Node nNode          = Listres.item(0);
            Element eElement    = (Element) nNode;
            String idPel = eElement.getElementsByTagName("idpel").item(0).getTextContent();
            String nmPel = eElement.getElementsByTagName("name").item(0).getTextContent();
            String swref = eElement.getElementsByTagName("switching_ref").item(0).getTextContent();
            String jBulan = eElement.getElementsByTagName("blth").item(0).getTextContent();
            String jAdmin = eElement.getElementsByTagName("biaya_admin").item(0).getTextContent();
            String jRpTag = eElement.getElementsByTagName("rp_tag").item(0).getTextContent();
            String jLunas = eElement.getElementsByTagName("waktu_lunas").item(0).getTextContent();
            
            //START CONVERT TO IMAGE
            convertImgPdam cImg = new convertImgPdam();
            cImg.startConvertFull(idPel,nmPel,swref,jBulan,jAdmin,jRpTag,jLunas);
            status = "sukses";
            
        }catch (IOException ex) {
            ex.printStackTrace();
            status = "gagal";
        }    
        
        return status;
    }
    
}
