/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionStrukPdam;

/**
 *
 * @author Djaya
 */
public class convertBlnThn {
    public static String findThBulan(String blth){
        String data = blth;
        String thn = data.substring(0,4);
        String bln = data.substring(Math.max(0, data.length() - 2));

        String findbln = findBulan(bln);
        
        String bulanTahun = findbln+thn;
        return bulanTahun;
    }
    
    private static String findBulan(String buln) {
        String bln = buln;
        String dayString = null;
        switch (bln) {
            case "01":
                dayString = "JAN ";
                break;
            case "02":
                dayString = "FEB ";
                break;
            case "03":
                dayString = "MAR ";
                break;
            case "04":
                dayString = "APR ";
                break;
            case "05":
                dayString = "MEI ";
                break;
            case "06":
                dayString = "JUN ";
                break;
            case "07":
                dayString = "JUL ";
                break;
            case "08":
                dayString = "AGS ";
                break;
            case "09":
                dayString = "SEP ";
                break;
            case "10":
                dayString = "OKT ";
                break;
            case "11":
                dayString = "NOV ";
                break;
            case "12":
                dayString = "DES ";
                break;
            default :
            dayString = "99";
        }
        return dayString;
    }
    public static String Bulan(String blth){
        String bulantahun = blth;
        String bulan = "";
        System.out.println("bulan tahun : "+bulantahun);
        String kode = bulantahun.substring(4, 6);
        System.out.println("kode : "+kode);
        if (kode.equalsIgnoreCase("01")) {
            bulan = "JAN";
        } else if (kode.equalsIgnoreCase("02")) {
            bulan = "FEB";
        } else if (kode.equalsIgnoreCase("03")) {
            bulan = "MAR";
        } else if (kode.equalsIgnoreCase("04")) {
            bulan = "APR";
        } else if (kode.equalsIgnoreCase("05")) {
            bulan = "MEI";
        } else if (kode.equalsIgnoreCase("06")) {
            bulan = "JUN";
        } else if (kode.equalsIgnoreCase("07")) {
            bulan = "JUL";
        } else if (kode.equalsIgnoreCase("08")) {
            bulan = "AUG";
        } else if (kode.equalsIgnoreCase("09")) {
            bulan = "SEP";
        } else if (kode.equalsIgnoreCase("10")) {
            bulan = "OKT";
        } else if (kode.equalsIgnoreCase("11")) {
            bulan = "NOV";
        } else if (kode.equalsIgnoreCase("12")) {
            bulan = "DES";
        } else{
            bulan = "DATANG BULAN";
        }
        System.out.println("bulan : "+bulan);
        //periode akhir bulan
        String bulan2;
        String kode2 = bulantahun.substring(10, 12);
        System.out.println("kode2 : "+kode);
        if (kode2.equalsIgnoreCase("01")) {
            bulan2 = "JAN";
        } else if (kode2.equalsIgnoreCase("02")) {
            bulan2 = "FEB";
        } else if (kode2.equalsIgnoreCase("03")) {
            bulan2 = "MAR";
        } else if (kode2.equalsIgnoreCase("04")) {
            bulan2 = "APR";
        } else if (kode2.equalsIgnoreCase("05")) {
            bulan2 = "MEI";
        } else if (kode2.equalsIgnoreCase("06")) {
            bulan2 = "JUN";
        } else if (kode2.equalsIgnoreCase("07")) {
            bulan2 = "JUL";
        } else if (kode2.equalsIgnoreCase("08")) {
            bulan2 = "AUG";
        } else if (kode2.equalsIgnoreCase("09")) {
            bulan2 = "SEP";
        } else if (kode2.equalsIgnoreCase("10")) {
            bulan2 = "OKT";
        } else if (kode2.equalsIgnoreCase("11")) {
            bulan2 = "NOV";
        } else if (kode2.equalsIgnoreCase("12")) {
            bulan2 = "DES";
        } else{
            bulan2 = "DATANG BULAN";
        }
        System.out.println("bulan2 : "+bulan);
        String tahun = bulantahun.substring(0, 4);
        System.out.println("tahun : "+tahun);
        String tahun2 = bulantahun.substring(6, 10);
        System.out.println("tahun2 : "+tahun2);
        return bulan;
    }
    public static String Bulan2(String blth){
        String bulantahun = blth;
        String bulan = "";
        System.out.println("bulan tahun : "+bulantahun);
        String kode = bulantahun.substring(4, 6);
        System.out.println("kode : "+kode);
        if (kode.equalsIgnoreCase("01")) {
            bulan = "JAN";
        } else if (kode.equalsIgnoreCase("02")) {
            bulan = "FEB";
        } else if (kode.equalsIgnoreCase("03")) {
            bulan = "MAR";
        } else if (kode.equalsIgnoreCase("04")) {
            bulan = "APR";
        } else if (kode.equalsIgnoreCase("05")) {
            bulan = "MEI";
        } else if (kode.equalsIgnoreCase("06")) {
            bulan = "JUN";
        } else if (kode.equalsIgnoreCase("07")) {
            bulan = "JUL";
        } else if (kode.equalsIgnoreCase("08")) {
            bulan = "AUG";
        } else if (kode.equalsIgnoreCase("09")) {
            bulan = "SEP";
        } else if (kode.equalsIgnoreCase("10")) {
            bulan = "OKT";
        } else if (kode.equalsIgnoreCase("11")) {
            bulan = "NOV";
        } else if (kode.equalsIgnoreCase("12")) {
            bulan = "DES";
        } else{
            bulan = "DATANG BULAN";
        }
        System.out.println("bulan : "+bulan);
        //periode akhir bulan
        String bulan2;
        String kode2 = bulantahun.substring(10, 12);
        System.out.println("kode2 : "+kode);
        if (kode2.equalsIgnoreCase("01")) {
            bulan2 = "JAN";
        } else if (kode2.equalsIgnoreCase("02")) {
            bulan2 = "FEB";
        } else if (kode2.equalsIgnoreCase("03")) {
            bulan2 = "MAR";
        } else if (kode2.equalsIgnoreCase("04")) {
            bulan2 = "APR";
        } else if (kode2.equalsIgnoreCase("05")) {
            bulan2 = "MEI";
        } else if (kode2.equalsIgnoreCase("06")) {
            bulan2 = "JUN";
        } else if (kode2.equalsIgnoreCase("07")) {
            bulan2 = "JUL";
        } else if (kode2.equalsIgnoreCase("08")) {
            bulan2 = "AUG";
        } else if (kode2.equalsIgnoreCase("09")) {
            bulan2 = "SEP";
        } else if (kode2.equalsIgnoreCase("10")) {
            bulan2 = "OKT";
        } else if (kode2.equalsIgnoreCase("11")) {
            bulan2 = "NOV";
        } else if (kode2.equalsIgnoreCase("12")) {
            bulan2 = "DES";
        } else{
            bulan2 = "DATANG BULAN";
        }
        System.out.println("bulan2 : "+bulan);
        String tahun = bulantahun.substring(0, 4);
        System.out.println("tahun : "+tahun);
        String tahun2 = bulantahun.substring(6, 10);
        System.out.println("tahun2 : "+tahun2);
        return bulan2;
    }
}
