/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionConvert;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author e-baddy
 */
public class findThBulan {   
    
    public static String findThBulan(String blth){
        String data = blth;
        String thn = data.substring(2,4);
        String bln = data.substring(Math.max(0, data.length() - 2));

        String findbln = findBulan(bln);
        
        String bulanTahun = findbln+thn;
        return bulanTahun;
    }
    
    private static String findBulan(String buln) {
        String bln = buln;
        String dayString = null;
        switch (bln) {
            case "01":
                dayString = "JAN";
                break;
            case "02":
                dayString = "FEB";
                break;
            case "03":
                dayString = "MAR";
                break;
            case "04":
                dayString = "APR";
                break;
            case "05":
                dayString = "MEI";
                break;
            case "06":
                dayString = "JUN";
                break;
            case "07":
                dayString = "JUL";
                break;
            case "08":
                dayString = "AGS";
                break;
            case "09":
                dayString = "SEP";
                break;
            case "10":
                dayString = "OKT";
                break;
            case "11":
                dayString = "NOV";
                break;
            case "12":
                dayString = "DES";
                break;
            default :
            dayString = "99";
        }
        return dayString;
    }
}
