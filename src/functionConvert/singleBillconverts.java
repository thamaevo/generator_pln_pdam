/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package functionConvert;


import static functionConvert.findThBulan.findThBulan;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author e-baddy
 */
public class singleBillconverts {

    public static StringBuilder startSingle(String freeText, String stan, String bAdmin) {

        //cek array
        String ary[] = freeText.split("\n");

        //split data freetext
        String data[] = ary[0].split("\\|");
        
        //kondisi cek
        String bills = "";
        bills = startBills(freeText);
        
        //send bulan
        findThBulan thBln = new findThBulan();
        List thbln = new ArrayList<String>();
        StringBuilder stanAw = new StringBuilder("");
        for (int i = 0; i < ary.length; i++) {
            String dt[] = ary[i].split("\\|");
            String getThbulan = findThBulan(dt[1]);
            
            //append to th bulan
            thbln.add(getThbulan);
            
            //append to stan awal akwir
            stanAw.append(dt[0]);
        }
        
        //varibel return for bulan tahun
        List thblnXml = new ArrayList<String>();
        thblnXml.addAll(thbln); 
        
        
        //total amount 
        int amount = Integer.parseInt(data[9]) + Integer.parseInt(bAdmin);
        
        
        //get rptag 
        String getRptag = getAmount(freeText);
        
        
        //build xml
        StringBuilder bXml = new StringBuilder("");
        bXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
        bXml.append("<response>");
        bXml.append("<produk> PLNPOSTPAID </produk>");
        bXml.append("<amount>" + amount + "</amount>");
        bXml.append("<stan>" + stan + "</stan>");
        bXml.append("<datetime>" + data[8].replace("-", "").replace(":", "").replace(" ", "") + "</datetime>");
        bXml.append("<date_settlement>00000000</date_settlement>");
        bXml.append("<merchant_code>6021</merchant_code>");
        bXml.append("<bank_code>4510017</bank_code>");
        bXml.append("<rc>0000</rc>");
        bXml.append("<terminal_id>0000000000000048</terminal_id>");
        bXml.append("<switcher_id>SWID003</switcher_id>");
        bXml.append("<bill_status>1</bill_status>");
        bXml.append("<outstanding_bill>01</outstanding_bill>");
        bXml.append("<payment_status>1</payment_status>");
        bXml.append("<subscriber_id>" + data[0].replace("-", "").trim() + "</subscriber_id>");
        bXml.append("<switcher_refno>"+data[7]+"</switcher_refno>");
        bXml.append("<subscriber_name>" + data[2] + " </subscriber_name>");
        bXml.append("<service_unit></service_unit>");
        bXml.append("<service_unit_phone>"+data[13]+"</service_unit_phone>");
        bXml.append("<subscriber_segmentation>"+data[3]+"</subscriber_segmentation>");
        bXml.append("<power>000000"+data[4]+"</power>");
        bXml.append("<admin_charge>" + bAdmin + "</admin_charge>");
        bXml.append("<info_text>000000000</info_text>");
        bXml.append("<bills>");
        bXml.append(bills);
        bXml.append("</bills>");
        bXml.append("<blth_summary>" + thblnXml.toString().replace("[", "").replace("]", "") + "</blth_summary>");
        bXml.append("<stand_meter_summary>00000000 - 00000000</stand_meter_summary>");
        bXml.append("<bk>0</bk>");
        bXml.append("<rptag>"+getRptag+"</rptag>");
        bXml.append("<saldo>0000000000</saldo>");
        bXml.append("<harga>00000</harga>");
        bXml.append("</response>");

        return bXml;
    }

    public static String startBills(String freeText) {
        //cek array
        String ary[] = freeText.split("\n");

        
        StringBuilder endXML = new StringBuilder("");
        System.out.println("jumlah array "+ (ary.length-1));
        for (int i = 0; i < ary.length; i++) {
            String data[] = ary[i].split("\\|");
            
            if (i != ary.length-1) {
                //build bills xml
                StringBuilder Bills = new StringBuilder("");
                Bills.append("<bill>");
                Bills.append("<bill_period>" + data[1] + "</bill_period>");
                Bills.append("<due_date></due_date>");
                Bills.append("<meter_read_date></meter_read_date>");
                Bills.append("<total_electricity_bill>000000" + data[9] + "</total_electricity_bill>");
                Bills.append("<incentive>00000000000</incentive>");
                Bills.append("<value_added_tax>0000000000</value_added_tax>");
                Bills.append("<penalty_fee>000000000000</penalty_fee>");
                Bills.append("<previous_meter_reading1>"+parseInt(data[6])+"</previous_meter_reading1>");
                Bills.append("<current_meter_reading1>"+parseInt(data[5])+"</current_meter_reading1>");
                Bills.append("<previous_meter_reading2>00000000</previous_meter_reading2>");
                Bills.append("<current_meter_reading2>00000000</current_meter_reading2>");
                Bills.append("<previous_meter_reading3>00000000</previous_meter_reading3>");
                Bills.append("<current_meter_reading3>00000000</current_meter_reading3>");
                Bills.append("</bill>");
                
                //apend tu xml end
                endXML.append(Bills);
            }else{
                //build bills xml
                StringBuilder Bills = new StringBuilder("");
                Bills.append("<bill>");
                Bills.append("<bill_period>" + data[1] + "</bill_period>");
                Bills.append("<due_date></due_date>");
                Bills.append("<meter_read_date></meter_read_date>");
                Bills.append("<total_electricity_bill>000000" + data[9] + "</total_electricity_bill>");
                Bills.append("<incentive>00000000000</incentive>");
                Bills.append("<value_added_tax>0000000000</value_added_tax>");
                Bills.append("<penalty_fee>000000000000</penalty_fee>");
                Bills.append("<previous_meter_reading1>"+parseInt(data[6])+"</previous_meter_reading1>");
                Bills.append("<current_meter_reading1>"+parseInt(data[5])+"</current_meter_reading1>");
                Bills.append("<previous_meter_reading2>00000000</previous_meter_reading2>");
                Bills.append("<current_meter_reading2>00000000</current_meter_reading2>");
                Bills.append("<previous_meter_reading3>00000000</previous_meter_reading3>");
                Bills.append("<current_meter_reading3>00000000</current_meter_reading3>");
                Bills.append("</bill>");
                
                //apend tu xml end
                endXML.append(Bills);
            }
        }
        
        return endXML.toString();

    }
    
    public static String getAmount(String freeText) {
        //cek array
        String ary[] = freeText.split("\n");

        
        StringBuilder endXML = new StringBuilder("");
        System.out.println("jumlah array "+ (ary.length-1));
        List getDt = new ArrayList<String>();
        
        double sum = 0;
        for (int i = 0; i < ary.length; i++) {
            String data[] = ary[i].split("\\|");
            getDt.add(data[9]);
            sum += parseInt(data[9]);
        }
        
        //hasil rp tag
        int tag = (int) sum;
        
        return String.valueOf(tag);
    }
}
